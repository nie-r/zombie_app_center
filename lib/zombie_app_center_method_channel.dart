import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

import 'zombie_app_center_platform_interface.dart';

/// An implementation of [ZombieAppCenterPlatform] that uses method channels.
class MethodChannelZombieAppCenter extends ZombieAppCenterPlatform {
  /// The method channel used to interact with the native platform.
  @visibleForTesting
  final methodChannel = const MethodChannel('zombie_app_center');

  @override
  Future<String?> getPlatformVersion() async {
    final version = await methodChannel.invokeMethod<String>('getPlatformVersion');
    return version;
  }
}
