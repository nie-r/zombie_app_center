import 'dart:io';

import 'package:flutter/services.dart';

import 'zombie_app_center_platform_interface.dart';

const _methodChannel = MethodChannel('zombie_app_center');

class ZombieAppCenter {
  Future<String?> getPlatformVersion() {
    try {
      return ZombieAppCenterPlatform.instance.getPlatformVersion();
    } catch (e) {
      return Future.value('');
    }
  }

  /// Start ZombieAppCenter functionalities
  static Future startAsync({
    required String appSecretAndroid,
    required String appSecretIOS,
    enableAnalytics = false,
    enableCrashes = false,
    enableDistribute = false,
    usePrivateDistributeTrack = false,
    disableAutomaticCheckForUpdate = false,
  }) async {
    String appsecret;
    if (Platform.isAndroid) {
      appsecret = appSecretAndroid;
    } else if (Platform.isIOS) {
      appsecret = appSecretIOS;
    } else {
      throw UnsupportedError('Current platform is not supported.');
    }

    if (appsecret.isEmpty) {
      return;
    }

    // WidgetsFlutterBinding.ensureInitialized();

    if (disableAutomaticCheckForUpdate) {
      await _disableAutomaticCheckForUpdateAsync();
    }

    await configureAnalyticsAsync(enabled: enableAnalytics);
    await configureCrashesAsync(enabled: enableCrashes);
    await configureDistributeAsync(enabled: enableDistribute);

    await _methodChannel.invokeMethod('start', <String, dynamic>{
      'secret': appsecret.trim(),
      'usePrivateTrack': usePrivateDistributeTrack,
    });
  }

  /// Track events
  static Future trackEventAsync(String name,
      [Map<String, String>? properties]) async {
    await _methodChannel.invokeMethod('trackEvent', <String, dynamic>{
      'name': name,
      'properties': properties ?? <String, String>{},
    });
  }

  /**
   * ========================================================================
   *                                                      V A L I D A C I O N
   * ========================================================================
   */

  /// Check whether analytics is enalbed
  static Future<bool?> isAnalyticsEnabledAsync() async {
    try {
      return await _methodChannel.invokeMethod('isAnalyticsEnabled');
    } catch (e) {
      stdout.writeln('Error checking isAnalyticsEnabled: $e');
      return false;
    }
  }

  /// Check whether crashes is enabled
  static Future<bool?> isCrashesEnabledAsync() async {
    try {
      return await _methodChannel.invokeMethod('isCrashesEnabled');
    } catch (e) {
      stdout.writeln('Error checking isCrashesEnabled: $e');
      return false;
    }
  }

  /// Check whether ZombieAppCenter distribution is enabled
  static Future<bool?> isDistributeEnabledAsync() async {
    try {
      return await _methodChannel.invokeMethod('isDistributeEnabled');
    } catch (e) {
      stdout.writeln('Error checking isDistributeEnabled: $e');
      return false;
    }
  }

  /**
   * ========================================================================
   *                                                C O N F I G U R A C I O N
   * ========================================================================
   */

  /// Enable or disable analytics
  static Future configureAnalyticsAsync({required enabled}) async {
    try {
      await _methodChannel.invokeMethod('configureAnalytics', enabled);
    } catch (e) {
      stdout.writeln(e);
    }
  }

  /// Enable or disable ZombieAppCenter crash reports
  static Future configureCrashesAsync({required enabled}) async {
    try {
      await _methodChannel.invokeMethod('configureCrashes', enabled);
    } catch (e) {
      stdout.writeln(e);
    }
  }

  /// Enable or disable ZombieAppCenter distribution
  static Future configureDistributeAsync({required enabled}) async {
    try {
      await _methodChannel.invokeMethod('configureDistribute', enabled);
    } catch (e) {
      stdout.writeln(e);
    }
  }

  /// Enable or disable ZombieAppCenter distribution for debug build (Android only)
  static Future configureDistributeDebugAsync({required enabled}) async {
    try {
      await _methodChannel.invokeMethod('configureDistributeDebug', enabled);
    } catch (e) {
      stdout.writeln(e);
    }
  }

  /**
   * ========================================================================
   *                                                                        N
   * ========================================================================
   */

  /// Get ZombieAppCenter installation id
  static Future<String> getInstallIdAsync() async {
    try {
      return await _methodChannel
          .invokeMethod('getInstallId')
          .then((r) => r as String);
    } catch (e) {
      return '';
    }
  }

  /// Disable automatic check for app updates
  static Future _disableAutomaticCheckForUpdateAsync() async {
    try {
      await _methodChannel.invokeMethod('disableAutomaticCheckForUpdate');
    } catch (e) {
      stdout.writeln(e);
    }
  }

  /// Manually check for app updates
  static Future checkForUpdateAsync() async {
    try {
      await _methodChannel.invokeMethod('checkForUpdate');
    } catch (e) {
      stdout.writeln(e);
    }
  }
}
