package com.cinisarboris.plugin.zombie_app_center;

import java.util.Map;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.embedding.engine.plugins.activity.ActivityAware;
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding;
import io.flutter.plugin.common.PluginRegistry.Registrar;

import android.app.Application;
import android.util.Log;
import androidx.annotation.NonNull;

import com.microsoft.appcenter.AppCenter;
import com.microsoft.appcenter.analytics.Analytics;
import com.microsoft.appcenter.crashes.Crashes;
import com.microsoft.appcenter.distribute.Distribute;
import com.microsoft.appcenter.distribute.UpdateTrack;

public class ZombieAppCenterPlugin implements FlutterPlugin, MethodCallHandler, ActivityAware {
    private static final String methodChannelName = "zombie_app_center";
    private static final String zombieInfo = "Zombie:Informacion";
    private static final String zombieError = "Zombie:Error";
    private String body = "default";
    private static Application application;
    private MethodChannel channel;

    @Override
    public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
        channel = new MethodChannel(flutterPluginBinding.getBinaryMessenger(), "zombie_app_center");
        channel.setMethodCallHandler(this);
    }

    @Override
    public void onMethodCall(@NonNull MethodCall call, @NonNull Result result) {
        // AppCenter.setLogLevel(Log.VERBOSE);
        AppCenter.setCountryCode("bo");

        try {
            switch (call.method) {

                /**
                 * ========================================================================
                 *                                                                  M A I N
                 * ========================================================================
                 */
                case "start": {
                    if (application == null) {
                        body = "[method:" + call.method + ":Fail to resolve Application on registration]";
                        Log.e(zombieError, body, new Exception(body));
                        result.error(zombieError, body, new Exception(body));
                        return;
                    }

                    String appSecret = call.argument("secret");
                    Boolean usePrivateTrack = call.argument("usePrivateTrack");
                    if (usePrivateTrack != null && usePrivateTrack) {
                        Distribute.setUpdateTrack(UpdateTrack.PRIVATE);
                    }

                    if (appSecret == null || appSecret.isEmpty()) {
                        body = "[method:" + call.method + ":App secret is not set]";
                        Log.e(zombieError, body, new Exception(body));
                        result.error(zombieError, body, new Exception(body));
                        return;
                    }

                    AppCenter.start(
                            application,
                            appSecret,
                            Analytics.class,
                            Crashes.class,
                            Distribute.class
                        );
                    break;
                }



                /**
                 * ========================================================================
                 *                                                    V A L I D A T I O N S
                 * ========================================================================
                 */

                case "isAnalyticsEnabled": {
                    try {
                        Log.i(zombieInfo, "executing: isAnalyticsEnabled");
                        result.success(Analytics.isEnabled().get());
                    } catch (Exception error) {
                        Log.e(zombieError, error.getMessage(), error);
                        result.error(zombieError, error.getMessage(), error);
                    }
                    return;
                }
                case "isCrashesEnabled": {
                    try {
                        Log.i(zombieInfo, "executing: isCrashesEnabled");
                        result.success(Crashes.isEnabled().get());
                    } catch (Exception error) {
                        Log.e(zombieError, error.getMessage(), error);
                        result.error(zombieError, error.getMessage(), error);
                    }
                    return;
                }
                case "isDistributeEnabled": {
                    try {
                        Log.i(zombieInfo, "executing: isDistributeEnabled");
                        result.success(Distribute.isEnabled().get());
                    } catch (Exception error) {
                        Log.e(zombieError, error.getMessage(), error);
                        result.error(zombieError, error.getMessage(), error);
                    }
                    return;
                }

                /**
                 * ========================================================================
                 */





                /**
                 * ========================================================================
                 *                                              C O N F I G U R A T I O N S
                 * ========================================================================
                 */

                case "configureCrashes": {
                    try {
                        Boolean value = (Boolean) call.arguments;
                        Log.i(zombieInfo, "executing: configureCrashes(" + value + ").get()");
                        Crashes.setEnabled(value).get();
                    } catch (Exception error) {
                        Log.e(zombieError, error.getMessage(), error);
                        result.error(zombieError, error.getMessage(), error);
                    }
                    break;
                }
                case "configureAnalytics": {
                    try {
                        Boolean value = (Boolean) call.arguments;
                        Log.i(zombieInfo, "executing: configureAnalytics(" + value + ").get()");
                        Analytics.setEnabled(value).get();
                    } catch (Exception error) {
                        Log.e(zombieError, error.getMessage(), error);
                        result.error(zombieError, error.getMessage(), error);
                    }
                    break;
                }
                case "configureDistribute": {
                    try {
                        Boolean value = (Boolean) call.arguments;
                        Log.i(zombieInfo, "executing: configureDistribute(" + value + ").get()");
                        Distribute.setEnabled(value).get();
                    } catch (Exception error) {
                        Log.e(zombieError, error.getMessage(), error);
                        result.error(zombieError, error.getMessage(), error);
                    }
                    break;
                }

                /**
                 * ========================================================================
                 */

                case "trackEvent": {
                    try {
                        Log.i(zombieInfo, "executing: trackEvent");
                        String name = call.argument("name");
                        Map<String, String> properties = call.argument("properties");
                        Analytics.trackEvent(name, properties);
                    } catch (Exception error) {
                        Log.e(zombieError, error.getMessage(), error);
                        result.error(zombieError, error.getMessage(), error);
                    }
                    break;
                }
                case "getInstallId": {
                    try {
                        Log.i(zombieInfo, "executing: getInstallId");
                        result.success(AppCenter.getInstallId().get().toString());
                    } catch (Exception error) {
                        Log.e(zombieError, error.getMessage(), error);
                        result.error(zombieError, error.getMessage(), error);
                    }
                    return;
                }
                case "configureDistributeDebug": {
                    try {
                        Log.i(zombieInfo, "executing: configureDistributeDebug");
                        Boolean value = (Boolean) call.arguments;
                        Distribute.setEnabledForDebuggableBuild(value);
                    } catch (Exception error) {
                        Log.e(zombieError, error.getMessage(), error);
                        result.error(zombieError, error.getMessage(), error);
                    }
                    break;
                }
                case "disableAutomaticCheckForUpdate": {
                    try {
                        Log.i(zombieInfo, "executing: disableAutomaticCheckForUpdate");
                        Distribute.disableAutomaticCheckForUpdate();
                    } catch (Exception error) {
                        Log.e(zombieError, error.getMessage(), error);
                        result.error(zombieError, error.getMessage(), error);
                    }
                    break;
                }
                case "checkForUpdate": {
                    try {
                        Log.i(zombieInfo, "executing: checkForUpdate");
                        Distribute.checkForUpdate();
                    } catch (Exception error) {
                        Log.e(zombieError, error.getMessage(), error);
                        result.error(zombieError, error.getMessage(), error);
                    }
                    break;
                }
                case "getPlatformVersion": {
                    try {
                        Log.i(zombieInfo, "executing: getPlatformVersion");
                        result.success("Android " + android.os.Build.VERSION.RELEASE);
                    } catch (Exception error) {
                        Log.e(zombieError, error.getMessage(), error);
                        result.error(zombieError, error.getMessage(), error);
                    }
                    return;
                }
                default: {
                    try {
                        result.notImplemented();
                    } catch (Exception error) {
                        Log.e(zombieError, error.getMessage(), error);
                        result.error(zombieError, error.getMessage(), error);
                    }
                    break;
                }
            }
            result.success(null);
        } catch (Exception error) {
            Log.e(zombieError, error.getMessage(), error);
            result.error(zombieError, error.getMessage(), error);
            throw error;
        }
    }

    @Override
    public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
        channel.setMethodCallHandler(null);
    }

    @Override
    public void onAttachedToActivity(ActivityPluginBinding binding) {
        application = binding.getActivity().getApplication();
    }

    @Override
    public void onDetachedFromActivity() {}

    @Override
    public void onDetachedFromActivityForConfigChanges() {}

    @Override
    public void onReattachedToActivityForConfigChanges(ActivityPluginBinding binding) {}

    public static void registerWith(Registrar registrar) {
        application = registrar.activity().getApplication();
        final MethodChannel channel = new MethodChannel(registrar.messenger(), methodChannelName);
        channel.setMethodCallHandler(new ZombieAppCenterPlugin());
    }
}
