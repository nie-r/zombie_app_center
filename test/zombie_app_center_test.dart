import 'package:flutter_test/flutter_test.dart';
import 'package:zombie_app_center/zombie_app_center.dart';
import 'package:zombie_app_center/zombie_app_center_platform_interface.dart';
import 'package:zombie_app_center/zombie_app_center_method_channel.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';

class MockZombieAppCenterPlatform
    with MockPlatformInterfaceMixin
    implements ZombieAppCenterPlatform {

  @override
  Future<String?> getPlatformVersion() => Future.value('42');
}

void main() {
  final ZombieAppCenterPlatform initialPlatform = ZombieAppCenterPlatform.instance;

  test('$MethodChannelZombieAppCenter is the default instance', () {
    expect(initialPlatform, isInstanceOf<MethodChannelZombieAppCenter>());
  });

  test('getPlatformVersion', () async {
    ZombieAppCenter zombieAppCenterPlugin = ZombieAppCenter();
    MockZombieAppCenterPlatform fakePlatform = MockZombieAppCenterPlatform();
    ZombieAppCenterPlatform.instance = fakePlatform;

    expect(await zombieAppCenterPlugin.getPlatformVersion(), '42');
  });
}
