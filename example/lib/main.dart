import 'package:flutter/material.dart';
import 'package:zombie_app_center/zombie_app_center.dart';
import 'package:zombie_app_center_example/appCenter/app_center_service.dart';
import 'dart:async';
import 'dart:math' as math;

import 'package:zombie_app_center_example/appCenter/custom_circular_progress_indicator.dart';
import 'package:zombie_app_center_example/appCenter/logger.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  Logger.logDebug('INIT - Future<void> main() async');
  const String secretAppCenter = "00000000-1111-2222-3333-444444444444";

  // await Future.delayed(Duration(seconds: 5 + (math.Random().nextInt(5) + 1)));

  bool distributionFlag = true;

  await ZombieAppCenter.startAsync(
    appSecretAndroid: secretAppCenter,
    appSecretIOS: secretAppCenter,
    enableAnalytics: true,
    enableCrashes: true,
    usePrivateDistributeTrack: true,
    disableAutomaticCheckForUpdate: false,
    enableDistribute: distributionFlag,
  );

  await ZombieAppCenter.configureDistributeDebugAsync(
      enabled: distributionFlag);

  // check if there is a new release available
  await ZombieAppCenter.checkForUpdateAsync();

  // check analytics status
  final analyticsEnabled = await ZombieAppCenter.isAnalyticsEnabledAsync();
  Logger.logInfo('Analytics enabled: $analyticsEnabled');

  // check crashes status
  final crashesEnabled = await ZombieAppCenter.isCrashesEnabledAsync();
  Logger.logInfo('Crashes enabled: $crashesEnabled');

  // check distribute status
  final distributeEnabled = await ZombieAppCenter.isDistributeEnabledAsync();
  Logger.logInfo('Distribute enabled: $distributeEnabled');

  Logger.logDebug('END - Future<void> main() async');

  await ZombieAppCenter.trackEventAsync('main() async');

  runApp(const MyApp());
}

fiveSecondsFuture() {
  return Future.delayed(Duration(seconds: 5 + (math.Random().nextInt(5) + 1)))
      .then((value) =>
          'Running on: ${DateTime.now().hour}:${DateTime.now().minute}:${DateTime.now().second}');
}

Future<void> configureAppCenter() async {}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final AppCenterService _appCenterService = AppCenterService();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Column(
          children: [
            Center(
              child: Text(
                'Time now: ${DateTime.now().hour}:${DateTime.now().minute}:${DateTime.now().second}',
                style: TextStyle(
                  fontSize: 20,
                  color: Color((math.Random().nextDouble() * 0xFFFFFF).toInt())
                      .withOpacity(1.0),
                ),
              ),
            ),
            _buildApp(),
          ],
        ),
      ),
    );
  }

  Widget _buildApp() {
    _appCenterService.trackEventAsync('buildApp()');

    // _appCenterService.checkForUpdateAsync();

    return Column(
      children: [
        const SizedBox(
          height: 20,
        ),
        _buildPlatformVersionWidget(),
        _buildFunctionalityCheckWidget(
            _appCenterService.isAnalyticsEnabledAsync(), 'Analytics'),
        _buildFunctionalityCheckWidget(
            _appCenterService.isCrashesEnabledAsync(), 'Crashes'),
        _buildFunctionalityCheckWidget(
            _appCenterService.isDistributeEnabledAsync(), 'Distribute'),
      ],
    );
  }

  Widget _buildPlatformVersionWidget() {
    return Center(
      child: FutureBuilder<String>(
        future: _appCenterService.getPlatformVersion(),
        builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const CustomCircularProgressIndicator();
          } else if (snapshot.hasError) {
            return const Text('Error al obtener la versión de la plataforma');
          } else {
            return Text(
              'Running on: ${snapshot.data}\n',
              style: const TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            );
          }
        },
      ),
    );
  }

  Widget _buildFunctionalityCheckWidget(Future<bool?> future, String title) {
    return FutureBuilder<bool?>(
      future: future,
      builder: (BuildContext context, AsyncSnapshot<bool?> snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Text('Checking $title...');
        } else if (snapshot.hasError) {
          return Text('$title check failed with error: ${snapshot.error}');
        } else {
          Logger.logInfo('Response Future<> $title: ${snapshot.data}');
          return Text(
              '$title is: ${snapshot.data == true ? 'Enabled' : 'Disabled'}');
        }
      },
    );
  }
}
