import 'dart:io';

import 'package:flutter/foundation.dart';

class Logger {
  static const _prefix = 'ExampleOnly';

  static void logDebug(String message) {
    if (kDebugMode) {
      print('$_prefix $message');
    }
  }

  static void logInfo(String message) {
    stdout.writeln('$_prefix $message');
  }

  static void logError(String message) {
    stderr.writeln('$_prefix $message');
  }
}
