import 'dart:math';
import 'package:flutter/material.dart';

class CustomCircularProgressIndicator extends StatelessWidget {
  final String text;

  const CustomCircularProgressIndicator({super.key, this.text = 'Cargando...'});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(
              Colors.primaries[Random().nextInt(Colors.primaries.length)],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 16.0),
            child: Text(
              text,
              style: TextStyle(
                fontSize: 24,
                color:
                    Colors.primaries[Random().nextInt(Colors.primaries.length)],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
