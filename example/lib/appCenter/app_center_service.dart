import 'package:zombie_app_center/zombie_app_center.dart';
import 'dart:math' as math;

import 'package:zombie_app_center_example/appCenter/logger.dart';

class AppCenterService {
  Future<String> getPlatformVersion() async {
    ZombieAppCenter zombieAppCenter = ZombieAppCenter();

    Logger.logInfo('Executing service : getPlatformVersion()');

    await Future.delayed(Duration(seconds: 5 + (math.Random().nextInt(5) + 1)));

    try {
      return await zombieAppCenter.getPlatformVersion() ?? 'Unknown';
    } catch (e) {
      Logger.logError('Executing ERR0R : getPlatformVersion: $e');
      return 'Failed to get platform version.';
    }
  }

  Future<bool?> isCrashesEnabledAsync() async {
    Logger.logInfo('Executing service : isCrashesEnabled()');

    await Future.delayed(Duration(seconds: 5 + (math.Random().nextInt(5) + 1)));

    try {
      return await ZombieAppCenter.isCrashesEnabledAsync();
    } catch (e) {
      Logger.logError('Executing ERR0R : isCrashesEnabledAsync: $e');
      return false;
    }
  }

  Future<bool?> isDistributeEnabledAsync() async {
    Logger.logInfo('Executing service : isDistributeEnabled()');

    await Future.delayed(Duration(seconds: 5 + (math.Random().nextInt(5) + 1)));

    try {
      return await ZombieAppCenter.isDistributeEnabledAsync();
    } catch (e) {
      Logger.logError('Executing ERR0R : isDistributeEnabledAsync: $e');
      return false;
    }
  }

  Future<bool?> isAnalyticsEnabledAsync() async {
    Logger.logInfo('Executing service : isAnalyticsEnabled()');

    await Future.delayed(Duration(seconds: 5 + (math.Random().nextInt(5) + 1)));

    try {
      return await ZombieAppCenter.isAnalyticsEnabledAsync();
    } catch (e) {
      Logger.logError('Executing ERR0R : isAnalyticsEnabledAsync: $e');
      return false;
    }
  }

  Future<void> checkForUpdateAsync() async {
    Logger.logInfo('Executing service : checkForUpdate()');

    await Future.delayed(Duration(seconds: 5 + (math.Random().nextInt(5) + 1)));

    try {
      await ZombieAppCenter.checkForUpdateAsync();
    } catch (e) {
      Logger.logError('Executing ERR0R : checkForUpdateAsync: $e');
    }
  }

  Future<void> trackEventAsync(String stringEvent) async {
    Logger.logInfo('Executing service : trackEventAsync()');

    await Future.delayed(Duration(seconds: 5 + (math.Random().nextInt(5) + 1)));

    try {
      await ZombieAppCenter.trackEventAsync(stringEvent);
    } catch (e) {
      Logger.logError('Executing ERR0R : trackEventAsync: $e');
    }
  }
}
